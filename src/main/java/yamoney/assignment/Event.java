package yamoney.assignment;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Объект  типизированного события в системе
 *
 * @see EventType
 * Created by Echo on 29.07.2017.
 */
@Data
@AllArgsConstructor
public class Event implements Serializable {

    private final EventType eventType;
    private final long timestamp;

}
