package yamoney.assignment;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * Тип события системы
 * Например, отправка фото в сервисе фотографий
 * Created by Echo on 29.07.2017.
 */
@Data
@AllArgsConstructor
public class EventType implements Serializable {

    private final String typeName;


}
