package yamoney.assignment.statistic;

import java.io.Serializable;

/**
 * Интерфейс объекта статистики
 * Предоставляет данные о количестве просшедших через сервис событий, по типу
 * <p>
 * Created by Echo on 29.07.2017.
 */
public interface IEventTypeStatistic extends Serializable {

    /**
     * Возвращает имя учитываемого типа
     *
     * @return имя типа
     * @see yamoney.assignment.EventType
     */
    String eventTypeName();

    /**
     * Возвращает количество событий произосшедших за последнюю минуту
     *
     * @return количество сообщений
     * @see yamoney.assignment.Event
     */
    long eventsPerMinute();

    /**
     * Возвращает количество событий произосшедших за последний час
     *
     * @return количество сообщений
     * @see yamoney.assignment.Event
     */
    long eventsPerHour();

    /**
     * Возвращает количество событий произосшедших за последние сутки
     *
     * @return количество сообщений
     * @see yamoney.assignment.Event
     */
    long eventsPerDay();

}
