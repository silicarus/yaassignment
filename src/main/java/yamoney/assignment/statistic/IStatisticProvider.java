package yamoney.assignment.statistic;

import yamoney.assignment.Event;

import java.util.concurrent.Future;

/**
 * Интерфес поставщика статистики по присшедшим событиям
 * @see Event
 * @see IEventTypeStatistic
 * <p>
 * Created by Echo on 29.07.2017.
 */
public interface IStatisticProvider {

    /**
     * Обработать событие
     *
     * @param event - событие
     * @return - статистика по типу опубликованного события
     * @see Event
     * @see IEventTypeStatistic
     */
    Future<IEventTypeStatistic> onEvent(final Event event);

}
