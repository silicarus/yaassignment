package yamoney.assignment.statistic;

import yamoney.assignment.Event;
import yamoney.assignment.EventType;
import yamoney.assignment.counters.BasicRangeCounter;
import yamoney.assignment.counters.IRangeCounter;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Реализация поставщика статистики по типизированным событиям
 * Created by Echo on 29.07.2017.
 */
public class StatisticProvider implements IStatisticProvider {

    /**
     * Реализация статистики
     *
     * @see IEventTypeStatistic
     */
    private static class EventTypeStatistic implements IEventTypeStatistic {

	private final IRangeCounter minute;
	private final IRangeCounter hour;
	private final IRangeCounter day;
	private final String typeName;

	private final Lock lock = new ReentrantLock();

	private EventTypeStatistic(String typeName, long initialTime) {
	    this.typeName = typeName;
	    minute = BasicRangeCounter.newMinuteRangeCounter(initialTime);
	    hour = BasicRangeCounter.newHourRangeCounter(initialTime);
	    day = BasicRangeCounter.newDayRangeCounter(initialTime);
	}

	private void update(long timestamp) {
	    lock.lock();
	    try {
		minute.inc(timestamp);
		hour.inc(timestamp);
		day.inc(timestamp);
	    } finally {
		lock.unlock();
	    }
	}

	@Override
	public String eventTypeName() {
	    return typeName;
	}

	@Override
	public long eventsPerMinute() {
	    lock.lock();
	    try {
		return minute.countByTime(System.currentTimeMillis());
	    } finally {
	        lock.unlock();
	    }
	}

	@Override
	public long eventsPerHour() {
	    lock.lock();
	    try {
		return hour.countByTime(System.currentTimeMillis());
	    } finally {
	        lock.unlock();
	    }
	}

	@Override
	public long eventsPerDay() {
	    lock.lock();
	    try {
		return day.countByTime(System.currentTimeMillis());
	    } finally {
	        lock.unlock();
	    }
	}
    }

    /**
     * Очередь для обработки событий
     */
    private final ExecutorService service = Executors.newSingleThreadExecutor();
    private final Map<String, EventTypeStatistic> statisticByType = new ConcurrentHashMap<>();

    @Override
    public Future<IEventTypeStatistic> onEvent(final Event event) {
	final EventType type = Objects.requireNonNull(event.getEventType(), "EventType is required");
	final String typeName = Objects.requireNonNull(type.getTypeName(), "TypeName is required");
	return service.submit(() -> {
	    EventTypeStatistic stat = statisticByType.get(typeName);
	    long eventTime = event.getTimestamp();
	    if (stat == null) {
		statisticByType.put(typeName, stat = new EventTypeStatistic(typeName, eventTime));
	    }
	    stat.update(eventTime);
	    return stat;
	});
    }

}
