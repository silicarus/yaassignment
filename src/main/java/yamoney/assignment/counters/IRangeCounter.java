package yamoney.assignment.counters;

/**
 * Интерфейс счетчика событий за период
 * <p>
 * Created by Echo on 29.07.2017.
 */
public interface IRangeCounter {

    /**
     * Увеличить значение для метки времени
     *
     * @param timestamp - метка времени события
     */
    void inc(long timestamp);

    /**
     * Получить количество значений на указанную метку времени
     * При нормальной работе - текущая метка времени
     *
     * @param timestamp - метка времени в милисекундах
     * @return - количество сообщений
     */
    long countByTime(long timestamp);

}
