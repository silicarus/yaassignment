package yamoney.assignment.counters;

import lombok.extern.log4j.Log4j;

import java.util.Arrays;
import java.util.Objects;

/**
 * ������� ���������� �������� �������, �� ����� �������
 * Created by Echo on 29.07.2017.
 */
@Log4j
public class BasicRangeCounter implements IRangeCounter {

    private static final int SECONDS_IN_MINUTE = 60;

    private static final int MINUTES_IN_HOUR = 60;

    private static final int HOURS_IN_DAY = 24;

    private static final long SECOND_SIZE = 1000L;

    private static final long MINUTE_SIZE = SECOND_SIZE * SECONDS_IN_MINUTE;

    private static final long HOUR_SIZE = MINUTE_SIZE * MINUTES_IN_HOUR;

    /**
     * ������ ����� ������� ������� �� ������
     *
     * @param timestamp - ����� �������, ������� ����� ��������������� �������
     * @return - ����� ��������� ��������
     */
    public static BasicRangeCounter newMinuteRangeCounter(long timestamp) {
	return new BasicRangeCounter(timestamp, SECOND_SIZE, SECONDS_IN_MINUTE, (ts) -> (ts / SECOND_SIZE) * SECOND_SIZE);
    }

    /**
     * ������ ����� ������� ������� �� ���
     *
     * @param timestamp - ����� �������, ������� ����� ��������������� �������
     * @return - ����� ��������� ��������
     */
    public static BasicRangeCounter newHourRangeCounter(long timestamp) {
	return new BasicRangeCounter(timestamp, MINUTE_SIZE, MINUTES_IN_HOUR, (ts) -> (ts / MINUTE_SIZE) * MINUTE_SIZE);
    }

    /**
     * ������ ����� ������� ������� �� ����
     *
     * @param timestamp - ����� �������, ������� ����� ��������������� �������
     * @return - ����� ��������� ��������
     */
    public static BasicRangeCounter newDayRangeCounter(long timestamp) {
	return new BasicRangeCounter(timestamp, HOUR_SIZE, HOURS_IN_DAY, (ts) -> (ts / HOUR_SIZE) * HOUR_SIZE);
    }

    /**
     * ��������������� ���������, ��� ������������ ����� �������
     */
    private interface ITruncateFunction {
	long truncate(long timestamp);
    }

    private long zeroTimestamp;
    private final long cellSize;
    private final int capacity;
    private final int lastCellIndex;
    private final ITruncateFunction truncateFunction;
    private final long[] cells;

    private BasicRangeCounter(long zeroTimestamp, long cellSize, int capacity, ITruncateFunction truncateFunction) {
	Objects.requireNonNull(truncateFunction, "Truncate function is undefined");
	if (cellSize < 0)
	    throw new IllegalArgumentException("Negative cell size");
	if (capacity < 0)
	    throw new IllegalArgumentException("Negative capasity");
	if (zeroTimestamp < 0)
	    throw new IllegalArgumentException("Negative timestamp");
	this.cellSize = cellSize;
	this.capacity = capacity;
	this.truncateFunction = truncateFunction;
	this.zeroTimestamp = truncateFunction.truncate(zeroTimestamp);
	cells = new long[capacity];
	lastCellIndex = capacity - 1;
    }

    /**
     * �������� ����� ������ �� ����� �������, ������� ���������� ���������
     *
     * @param time - ����� �������
     * @return - ����� ������
     */
    private int timestampToCell(long time) {
	return Math.toIntExact((time - zeroTimestamp) / cellSize);
    }

    /**
     * �������� �������� (�������� ���������� ��������)
     * @param offset - ������ ������
     */
    private void shiftCells(int offset) {
	if (log.isDebugEnabled()) log.debug("Shift arrays: " + offset);
	int size = capacity - offset;
	System.arraycopy(cells, offset, cells, 0, size);
	for (int i = size; i < capacity; i++) {
	    cells[i] = 0;
	}
    }

    @Override
    public void inc(long timestamp) {
	final long truncated = truncateFunction.truncate(timestamp);
	if (truncated < zeroTimestamp)
	    return; // �������� ��� ��������
	int cell = timestampToCell(timestamp);
	if (cell >= capacity) {
	    int offset = cell - capacity + 1;
	    if (offset < capacity) {
		// ����� �������� �������� ������
		shiftCells(offset);
		cell = capacity - offset;
		zeroTimestamp += offset * cellSize;
		if (log.isDebugEnabled()) log.debug("set timestamp to: " + zeroTimestamp);
	    } else {
		// ������ ��������� ������ ������
		if (log.isDebugEnabled()) log.debug("Clean all data");
		Arrays.fill(cells, 0L);
		zeroTimestamp = truncated;
		if (log.isDebugEnabled()) log.debug("set timestamp to: " + zeroTimestamp);
		cell = 0;
	    }
	}
	cells[cell]++;
    }

    @Override
    public long countByTime(long timestamp) {
	final long truncated = truncateFunction.truncate(timestamp);
	int endCell = timestampToCell(truncated);
	int beginCell = endCell - capacity;
	long result = 0;
	if (log.isDebugEnabled()) log.debug("get from range: " + beginCell + "-" + endCell);
	if (beginCell < capacity && endCell >= 0) {
	    // ����� ������� ������� � �������� ��������
	    beginCell = Math.max(beginCell, 0); // ��������� ������ ��� ��������� ��������
	    endCell = Math.min(lastCellIndex, endCell); // �������� ������ ��� ��������� ��������
	    for (int i = beginCell; i <= endCell; i++) {
		result += cells[i];
	    }
	} else if (log.isDebugEnabled()) {
	    log.debug("Out of range: " + truncated + ", minimal: " + zeroTimestamp);
	}
	return result;
    }

}
