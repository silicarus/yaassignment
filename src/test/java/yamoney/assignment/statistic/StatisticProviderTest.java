package yamoney.assignment.statistic;

import lombok.extern.log4j.Log4j;
import org.junit.Assert;
import org.junit.Test;
import yamoney.assignment.Event;
import yamoney.assignment.EventType;

import java.util.Objects;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;

/**
 * Тест поставщика статистики
 *
 * @see IEventTypeStatistic
 * Created by Echo on 29.07.2017.
 */
@Log4j
public class StatisticProviderTest {

    @Test
    public void statisticTest() {
	final StatisticProvider provider = new StatisticProvider();
	final EventType type = new EventType("testEventType");
	Future<IEventTypeStatistic> selector = null;
	final int expectedMessages = 424242;
	log.info("Expected events count: " + expectedMessages);
	for (int i = 0; i < expectedMessages; i++)
	    selector = provider.onEvent(new Event(type, System.currentTimeMillis())); // last modification
	try {
	    IEventTypeStatistic statistic = Objects.requireNonNull(selector.get(), "Null result");
	    assertEquals(expectedMessages, statistic.eventsPerMinute());
	    assertEquals(expectedMessages, statistic.eventsPerHour());
	    assertEquals(expectedMessages, statistic.eventsPerDay());
	    assertEquals(type.getTypeName(), statistic.eventTypeName());
	} catch (Throwable ex) {
	    log.error("Get statistic error", ex);
	    Assert.fail("Get statistic error: " + ex.getLocalizedMessage());
	}
    }

}