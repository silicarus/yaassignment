package yamoney.assignment.counters;

import org.junit.Assert;
import org.junit.Test;

/**
 * Тест счетчика событий
 *
 * @see IRangeCounter
 * Created by Echo on 29.07.2017.
 */
public class BasicRangeCounterTest {

    private static final int SECONDS_IN_MINUTE = 60;

    private static final int MINUTES_IN_HOUR = 60;

    private static final int HOURS_IN_DAY = 24;

    private static final long SECOND_SIZE = 1000L;

    private static final long MINUTE_SIZE = SECOND_SIZE * SECONDS_IN_MINUTE;

    private static final long HOUR_SIZE = MINUTE_SIZE * MINUTES_IN_HOUR;

    private static final long TEN_MINUTES = MINUTE_SIZE * 10;

    private static final long TEN_HOURS = HOUR_SIZE * 10;

    private static final long ONE_DAY_IN_MINUTES = HOURS_IN_DAY * HOUR_SIZE;

    private static long truncateMillis(long ts) {
	return (ts / SECOND_SIZE) * SECOND_SIZE;
    }

    private static long truncateMinutes(long ts) {
	return (ts / MINUTE_SIZE) * MINUTE_SIZE;
    }

    private static long truncateHours(long ts) {
	return (ts / HOUR_SIZE) * HOUR_SIZE;
    }

    @Test
    public void minuteRangeCounterTest() throws Exception {
	final long initialTime = truncateMillis(System.currentTimeMillis());
	final long endTime = initialTime + TEN_MINUTES; // Ten minutes
	IRangeCounter counter = BasicRangeCounter.newMinuteRangeCounter(initialTime);
	long time = initialTime;
	while ((endTime - time) > 0) {
	    time += 2000L; // 1 сообщений в 2 секунды (30 всего должно быть)
	    counter.inc(time);
	}
	Assert.assertEquals(30, counter.countByTime(endTime));
	Assert.assertEquals(0, counter.countByTime(initialTime));
	Assert.assertEquals(0, counter.countByTime(endTime + TEN_MINUTES));
    }

    @Test
    public void hourRangeCounterTest() throws Exception {
	final long initialTime = truncateHours(System.currentTimeMillis());
	final long endTime = initialTime + TEN_HOURS; // Ten hours
	IRangeCounter counter = BasicRangeCounter.newHourRangeCounter(initialTime);
	long time = initialTime;
	while ((endTime - time) > 0) {
	    counter.inc(time);
	    time += MINUTE_SIZE; // Одно сообщение в минуту
	}
	Assert.assertEquals(SECONDS_IN_MINUTE, counter.countByTime(endTime));
    }

    @Test
    public void dayRangeCounterTest() throws Exception {
	final long initialTime = truncateMinutes(System.currentTimeMillis());
	final long endTime = initialTime + ONE_DAY_IN_MINUTES; // One day
	IRangeCounter counter = BasicRangeCounter.newDayRangeCounter(initialTime);
	long time = initialTime;
	while ((endTime - time) > 0) {
	    counter.inc(time);
	    time += HOUR_SIZE; // Одно сообщение в час
	}
	Assert.assertEquals(HOURS_IN_DAY, counter.countByTime(endTime));
    }


}